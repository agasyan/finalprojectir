from django.shortcuts import render, redirect
from .models import place
import json
import math
#Deskripsi , teknik, solusi, hasil, kesimpulan
def index(request):
    return render(request, 'index.html')

def search_attr(request):
    if request.method == 'POST':
        query = request.POST['query']
        city = request.POST['city']
        queryRes = {'query': query,'city':city}
        status = None
        list = place.objects.all().values()
        list_len = len(list)
        (full_idx, df_idx, weight_idx) = data_index(list)
        
        i = 0
        for elem in full_idx:
            print("doc %d" %(i))
            for elem2 in elem:
                print("TF %s : %s" %(elem2, elem[elem2]))
            i = i + 1
            print("")
        print("")
        for elem in df_idx:
            print("DF %s: %d" %(elem, df_idx[elem]))
        print("")
        
        i = 0
        for elem in weight_idx:
            print("doc %d" %(i))
            for elem2 in elem:
                print("WEIGHT %s : %s" %(elem2, elem[elem2]))
            i = i + 1
            print("")
        for elem in list:
            print(elem)

        idx_arr_res = get_index_sorted(query, weight_idx, df_idx)
        for elem in idx_arr_res:
            print("SORT %d" %(elem))

        sorted_list = []
        for idx in idx_arr_res:
            sorted_list.append(list[idx])
            
        tmp_list = []
        i = 0
        for elem in sorted_list:
            if i >= 4:
                break
            
            if city in elem['address']:
                print(elem['name'])
                tmp_list.append(elem)
                
            i = i + 1
        #for i in range(len(list)):
        #    if city in (list[i]['address']):
        #        #list[i]['lol'] = 10
        #        print(list[i]['name'])
        #        tmp_list.append(list[i])
        #    else:
        #        continue
        place_dict = {'places':tmp_list}
        if len(tmp_list) == 0:
            status = {'noCity' : True}
        else:
            status = {'noCity' : False}
        ctx = {**queryRes, **status, **place_dict}
        return render (request, 'result.html', ctx)
    return redirect('/retrieval')

def data_index(data):
    full_idx = []
    df_hash = {}
    weight_arr = []
    data_len = len(data)
    for elem in data:
        tf_hash = {}
        found_hash = {}
        do_index(elem, 'name', 10, tf_hash, df_hash, found_hash, 1)
        do_index(elem, 'address', 3, tf_hash, df_hash, found_hash, 2)
        do_index(elem, 'description', 1, tf_hash, df_hash, found_hash, 3)
        full_idx.append(tf_hash)
        
    for elem in full_idx:
        weight_hash = {}
        for key in elem:
            weight_hash[key] = elem[key] * (math.log(3 * data_len / df_hash[key]) / math.log(10))
        weight_arr.append(weight_hash)
    return (full_idx, df_hash, weight_arr)

def do_index(data, attribute, value, tf, df, found_hash, found_df_value):
    attr_val = data[attribute]
    split_list = attr_val.split(' ')
    for token in split_list:
        token = token.lower()
        token_former = token
        
        if not(token in tf):
            tf[token] = 0
        if not(token in found_hash):
            if not(token in df):
                df[token] = 0
            df[token] = df[token] + found_df_value
            found_hash[token] = True   
        tf[token] = tf[token] + value
    return 0

def query_to_hash_idx(query, df_hash, data_len):
    freq = {}
    weight = {}
    query_split = query.split(' ')
    for elem in query_split:
        elem = elem.lower()
        if not(elem in freq):
            freq[elem] = 0
        freq[elem] = freq[elem] + 3

    for elem in freq:
        if elem in df_hash:
            weight[elem] = freq[elem] * (math.log(3 * data_len / df_hash[elem]) / math.log(10))
    return (freq, weight)

def get_index_sorted(query, weight_idx, df_idx):
    data_len = len(weight_idx)
    (query_freq, query_weight) = query_to_hash_idx(query, df_idx, data_len)

    weight_list = []
    for i in range(data_len):
        weight_list.append(cosine_similarity(query, weight_idx, df_idx, i))

    (tmp, idx_arr) = bubble_sort(weight_list)
    return idx_arr

def bubble_sort(arr):
    arr_len = len(arr)
    idx_arr = []

    for i in range(arr_len):
        idx_arr.append(i)

    for i in range(arr_len):
        for j in range(0, arr_len - i - 1):
            if(arr[j] < arr[j+1]):
                arr[j], arr[j+1] = arr[j+1], arr[j]
                idx_arr[j], idx_arr[j+1] = idx_arr[j+1], idx_arr[j]
    return (arr, idx_arr)

def cosine_similarity(query, weight_idx, df_idx, idx):
    weight_hash = weight_idx[idx]
    data_len = len(weight_idx)
    (query_freq, query_weight) = query_to_hash_idx(query, df_idx, data_len)

    euclid_sum_query = 0
    for elem in query_weight:
        euclid_sum_query = euclid_sum_query + query_weight[elem] * query_weight[elem]

    euclid_sum_corpus = 0
    for elem in weight_hash:
        euclid_sum_corpus = euclid_sum_corpus + weight_hash[elem] * weight_hash[elem]

    cross_product_sum = 0
    for elem in query_weight:
        if elem in weight_hash:
            cross_product_sum = cross_product_sum + query_weight[elem] * weight_hash[elem]
    if math.sqrt(euclid_sum_query) * math.sqrt(euclid_sum_corpus) == 0.0:
        return 0
    else:
        return cross_product_sum / (math.sqrt(euclid_sum_query) * math.sqrt(euclid_sum_corpus))
