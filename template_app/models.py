from django.db import models

# Create your models here.
class place(models.Model):
    name = models.CharField(max_length=50)
    open_hours = models.CharField(max_length=60)
    address = models.CharField(max_length=200) 
    description = models.TextField() 
    img_URL = models.TextField() 
