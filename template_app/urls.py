from django.urls import re_path
from .views import index, search_attr
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'search/$', search_attr, name='search_attr')
]
