function validateForm() {
  var x = document.forms["input-form"]["query"].value;
  var city = document.forms["input-form"]["city-select"].value;
  console.log(x);
  console.log(city);
  if (x == "" && city == "0") {
    alert("Query must be filled out and city must be choosen");
    return false;
  } else if (x == "") {
    alert("Query must be filled out");
    return false;
  } else if (city == "0") {
    alert("City must be choosen");
    return false;
  }
}

$(document).ready(function(){
    // Add smooth scrolling to all links in navbar + footer link
    $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();
  
        // Store hash
        var hash = this.hash;
  
        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 900, function(){
     
          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
      } // End if
    });
    
    $(window).scroll(function() {
      $(".slideanim").each(function(){
        var pos = $(this).offset().top;
  
        var winTop = $(window).scrollTop();
          if (pos < winTop + 600) {
            $(this).addClass("slide");
          }
      });
    });
  })