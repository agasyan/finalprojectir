# Information Retrieval Final Project

## Nama dan Anggota Kelompok
Nama Kelompok: kIRito-D
- Agas Yanpratama - 1606918396
- Jahns Christian A. - 1506758014

## Topic of Project
Retrieval Model dari tempat wisata di Indonesia 

## Desirable Usecase
User yang menggunakan mengisi 2 query yaitu kota tempat wisata dan query yang menunjukkan apa yang ingin dicari output berupa top-k dokumen yang berisi nama tempat wisata dan deskripsi tempat wisatanya.

## How To Use

1. Masukkan Kota yang diinginkan pada field kota.

2. Masukkan query yang diinginkan pada text box query.

3. Klik tombol search dan tempat wisata yang sesuai akan ditampilkan.

## Data Sets
_Data Crawling_ dari _google maps_ dan _Wikipedia_.

## Poster
![SIWISATA Poster](https://gitlab.com/agasyan/finalprojectir/raw/master/poster.png "SIWISATA Poster")

## Deployment
deployed to: http://ir-attraction.herokuapp.com/ 

source code: https://gitlab.com/agasyan/finalprojectir

## Run Locally

Catatan: Karena basis data (korpus) sistem kami dari Internet (Postgre Heroku), maka untuk menjalankan secara lokal harus ada akses Internet untuk menjalankan sistem kami.

1. Buat sebuah **virtual environment** untuk proyek ini dengan perintah berikut:

    ```bash
    python -m venv env
    ```
2. Aktifkan **virtual environment** untuk menginstal dependensi yang dibutuhkan untuk menjalankan proyek ini:

    Windows:
    
    ```bash
    env\Scripts\activate.bat
    pip install -r requirements.txt
    ```

    Linux & Mac OS:

    ```bash
    source env/bin/activate
    pip install -r requirements.txt
    ```

3. Jalankan secara lokal dengan perintah:

    ```bash
    python manage.py runserver 8000
    ```

4. Akses website secara lokal pada  `http://localhost:8000`

5. Jika sudah selesai untuk mematikan **virtual environment** gunakan perintah:

    ```bash
    deactivate
    ```
